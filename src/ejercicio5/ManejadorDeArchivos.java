package ejercicio5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ManejadorDeArchivos {
    
    
    private File f;
    private File ruta;

    public ManejadorDeArchivos(String dir, String filename) {
        ruta = new File(dir);
        f = new File(ruta, filename);
    }
    
    public File locateFile() throws IOException {

        System.out.println(f.getAbsolutePath());
        System.out.println(f.getParent());
        System.out.println(ruta.getAbsolutePath());
        System.out.println(ruta.getParent());

        if (!f.exists()) { 
            System.out.println("Fichero " + f.getName() + " no existe");
            if (!ruta.exists()) { 
                System.out.println("El directorio " + ruta.getName() + " no existe");
                if (ruta.mkdir()) { 

                    System.out.println("Directorio creado");

                    if (f.createNewFile()) { 
                        System.out.println("Fichero " + f.getName() + " creado");
                    } else {
                        System.out.println("No se ha podido crear " + f.getName());
                    }
                } else {
                    System.out.println("No se ha podido crear " + ruta.getName());
                }
            } else { 
                if (f.createNewFile()) {
                    System.out.println("Fichero " + f.getName() + " creado");
                } else {
                    System.out.println("No se ha podido crear " + f.getName());

                }
            }

        } else { 
            System.out.println("Fichero " + f.getName() + " existe");
            System.out.println("Tamaño " + f.length() + " bytes");
        }
        return f;
    }
    
    
    public void write(String text) {
        try {
            FileWriter escribir = new FileWriter(f, true);
            
            escribir.write(text);
            escribir.close();
        } catch (IOException error) {
            System.out.println("Error al escribir");
            error.printStackTrace();
        }
    }
    
    
    public void read() {
        try {
            Scanner reader = new Scanner(f);
            
            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                System.out.println(data);
            }
               
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Hubo un problema");
            e.printStackTrace();
        }
    }
}