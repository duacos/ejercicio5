
package ejercicio5;

import java.io.IOException;

/**
 *
 * @author duacos
 */
public class Ejercicio5 {

    static String direccionEnDisco = "c:\\actividad"; 
    static String nombreDeArchivo = "actividad4.txt";
    
    public static void main(String[] args) throws IOException {
        
        
        // punto 1 - Escribir archivo
        
        ManejadorDeArchivos file = new ManejadorDeArchivos(direccionEnDisco, nombreDeArchivo);
        
        file.locateFile();
        file.write("Código de estudiante: 4151620021 \n" + 
                   "Nombres y apellidos: Jorge Iván Durango Acosta \n" +  
                   "Edad: 23 \n" + 
                   "Asignatura: POO");
        
        
        // punto 2 - leer el archivo que se creó en el punto 1
        System.out.println("---------------------------------------------------------");
        file.read();
    }
    
}
